﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SberWpf.Binding
{
    class IntegerValidation : ValidationRule
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int parsedValue = 0;
            try
            {
                if (((string)value).Length > 0)
                    parsedValue = Int32.Parse((String)value);
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Недопустимые символы");
            }

            if ((parsedValue < Min) || (parsedValue > Max))
            {
                return new ValidationResult(false,
                  "Значение должно быть в промежутке от " + Min + " до " + Max + ".");
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}
