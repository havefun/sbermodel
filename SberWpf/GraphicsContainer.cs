﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using SberModel.Interfaces;

namespace SberWpf
{
    public class GraphicsContainer : IDrawer
    {
        public Line DrawLine(Point startPoint, Point endPoint)
        {
            var line = new Line
            {
                Stroke = Brushes.LightSteelBlue,
                X1 = startPoint.X,
                X2 = endPoint.X,
                Y1 = startPoint.Y,
                Y2 = endPoint.Y,
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                StrokeThickness = 2
            };
            return line;
        }

        public Rectangle DrawRectangle(Point position, int width, int height)
        {
            var rectangle = new Rectangle();
            rectangle.Width = width - 2;
            rectangle.Height = height - 2;
            rectangle.Fill = Brushes.Green;
            Canvas.SetLeft(rectangle, position.X);
            Canvas.SetTop(rectangle, position.Y);
            return rectangle;
        }

        public Ellipse DrawEllipse(int width, int height)
        {
            var ellipse = new Ellipse();
            ellipse.Width = width;
            ellipse.Height = height;
            ellipse.Fill = Brushes.CornflowerBlue;
            Canvas.SetRight(ellipse, 0);
            Canvas.SetBottom(ellipse, 0);
            return ellipse;
        }

        public TextBlock DrawText(string text, Point position, int width, int height)
        {
            var textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.TextAlignment = TextAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Center;
            textBlock.Width = width;
            textBlock.Height = height;
            textBlock.FontSize = 18;
            textBlock.FontWeight = FontWeight.FromOpenTypeWeight(500);
            textBlock.Foreground = Brushes.White;
            if (position.X == 0)
                Canvas.SetRight(textBlock, 0);
            else
                Canvas.SetLeft(textBlock, position.X);

            if (position.Y == 0)
                Canvas.SetBottom(textBlock, 0);
            else
                Canvas.SetTop(textBlock, position.Y);
            return textBlock;
        }
    }
}
