﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;
using SberModel;
using SberModel.Models;

namespace SberWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public List<Rectangle> Operators = new List<Rectangle>();

        public MainWindow()
        {
            InitializeComponent();
            Communication.NewMessageReceived += Communication_NewMessageReceived;       
        }

        void Communication_NewMessageReceived(object sender, NewMessageReceivedEventArgs e)
        {
            SetTextCallBack callBack = SetText;
            if (sender is VisibleElement)
            {
                var el = sender as VisibleElement;
                UpdateViewCallBack update = el.UpdateView;
                Dispatcher.BeginInvoke(update, e.Message.Status);
                ActWithCanvas actElement = AddElement;
                if (e.Message.Status == ModelStatus.ClientFinished)
                    actElement = RemoveElement;
                Dispatcher.BeginInvoke(actElement, el.GraphicPresentation);
            }
            Dispatcher.BeginInvoke(callBack,
                string.Format("{0}: {1}{2}", DateTime.Now, e.Message.Text,
                    Environment.NewLine));
        }

        private void SetText(string text)
        {
            OutputText.Text += text;
        }

        private void AddElement(UIElement element)
        {
            if (element != null && !Grid.Children.Contains(element))
                Grid.Children.Add(element);
        }

        private void RemoveElement(UIElement element)
        {
            if (Grid.Children.Contains(element))
                Grid.Children.Remove(element);
        }
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                //var rect = Model.Drawer.DrawRectangle(new Point(i*50 + 10, 10), 30, 30);
                //Operators.Add(rect);
                //Grid.Children.Add(rect);
            }

            if (!Model.IsInitialized)
                Model.Initialize(new GraphicsContainer());
            if (!Model.IsActive)
                Model.Start();
        }

        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {
            if (Model.IsActive)
                Model.Pause();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Model.IsActive)
                Model.Pause();
        }
    }

    internal delegate void SetTextCallBack(string text);

    internal delegate void UpdateViewCallBack(ModelStatus status);

    internal delegate void ActWithCanvas(UIElement element);
}
