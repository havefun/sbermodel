﻿using System;

namespace SberModel.Interfaces
{
    public interface IMessageUser
    {
        Type GetSenderType();
    }
}
