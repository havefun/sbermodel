﻿namespace SberModel
{
    interface IOperator
    {
        bool CallNextClient();
        void ServeClient();
        bool GetFromServer();
        bool SaveToServer();
    }
}
