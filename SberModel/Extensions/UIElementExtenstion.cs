﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SberModel.Extensions
{
    public static class UIElementExtenstion
    {
        public static Point GetPosition(this UIElement element)
        {
            Vector vec = VisualTreeHelper.GetOffset(element);
            return new Point(vec.X, vec.Y);
        }
    }
}
