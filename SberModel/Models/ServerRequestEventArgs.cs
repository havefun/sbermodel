﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel.Models
{
    public class ServerRequestEventArgs : EventArgs
    {
        public double TaskCoefficient;

        public ServerRequestEventArgs(double taskCoefficient)
        {
            TaskCoefficient = taskCoefficient;
        }
    }
}
