﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SberModel.Models;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Controls;

namespace SberModel
{
    public enum ServerStatus
    {
        Waiting,
        Busy
    }

    public class TaskDoneEventArgs : EventArgs
    {
        public readonly long OwnerId;

        public TaskDoneEventArgs(long id)
        {
            OwnerId = id;
        }
    }
    public class Server : VisibleElement
    {
        public static event EventHandler<TaskDoneEventArgs> TaskDone;
        private readonly Department _department;
        private ConcurrentQueue<ServerTask> _tasks = new ConcurrentQueue<ServerTask>();
        public ServerStatus Status { get; private set; }
        public ServerTask CurrentTask { get; private set; }

        public Server(Department department)
        {
            _department = department;
            Status = ServerStatus.Waiting;
            GraphicPresentations.Add(Model.Drawer.DrawRectangle(new Point(5, 5), 40, 50));
            GraphicPresentations.Add(Model.Drawer.DrawText("", new Point(5, 5), 40, 50));
            StartServer();
            Subscribe();
        }

        private void StartServer()
        {
            foreach (var op in _department.Operators)
            {
                op.NewServerRequest += NewServerRequest;
            }
            Send(ModelStatus.ServerCreated);
        }

        protected override void ModelTimeChanged(object sender, TimeChangedEventArgs e)
        {
            if (Status == ServerStatus.Waiting)
            {
                Send(ModelStatus.ServerWaiting);
                Model.ServerLoad = e.NewValue - e.LastValue;
                ServerTask task;
                if (_tasks.TryDequeue(out task))
                {
                    Unsubcribe();
                    DoTask(task);
                    Subscribe();
                }
            }
            else if (Status == ServerStatus.Busy && NextEventTime <= e.NewValue)
            {
                Unsubcribe();
                Status = ServerStatus.Waiting;
                //Send("Сервер: завершение задания от операциониста {0}", CurrentTask.OwnerOperator.Number);
                SendResponse();
                Subscribe();
            }
        }

        void NewServerRequest(object sender, Models.ServerRequestEventArgs e)
        {
            var task = new ServerTask()
            {
                OwnerOperator = (sender as Operator),
                ReqiuredTimeCoeff = e.TaskCoefficient
            };
            //Send("Сервер: получено новое задание от операциониста {0}", task.OwnerOperator.Number);
            _tasks.Enqueue(task);
        }

        private void SendResponse()
        {
            if (TaskDone != null)
            {
                TaskDone(this, new TaskDoneEventArgs(CurrentTask.OwnerOperator.Id));
            }
            //CurrentTask.OwnerOperator.GetServerResponse();
        }
        private void DoTask(ServerTask task)
        {
            NextEventTime = CurrentModelTime + RandomGenerator.NextNormal(((double) Model.TaskTime)/60*task.ReqiuredTimeCoeff, 0.5d);
            CurrentTask = task;
            Status = ServerStatus.Busy;
            Send(ModelStatus.ServerWorking);
        }

        public override void UpdateView(ModelStatus status)
        {
            Rectangle rect = (Rectangle)GraphicPresentations[0];
            switch (status)
            {
                case ModelStatus.ServerWorking:
                    {
                        rect.Fill = Brushes.Orange;
                        (GraphicPresentations[1] as TextBlock).Text = CurrentTask.OwnerOperator.Number.ToString();
                        break;
                    }
                default:
                    {
                        rect.Fill = Brushes.GreenYellow;
                        (GraphicPresentations[1] as TextBlock).Text = "";
                        break;
                    }
            }
        }
    }
}
