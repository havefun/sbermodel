﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel
{
    public static class RandomGenerator
    {
        private static Random _random = new Random();

        public static double NextNormal(double mean, double variance)
        {
            return mean + variance*_random.NextGaussian();
        }
    }
}
