﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SberModel.Models;

namespace SberModel
{
    public class Department : ModelListener
    {
        private static Department _instance;

        public static Department Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Department();
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        private Department()
        {
            Operators = new List<Operator>();
            for (int i = 0; i < Model.Operators; i++)
            {
                Operators.Add(new Operator(this, i+1));
            }
            Queue = new Queue();
            Server = new Server(this);
            NextEventTime = CurrentModelTime + RandomGenerator.NextNormal(((double) 60 / Model.ClientsPerHour), 0.8d);
            Subscribe();
        }

        public List<Operator> Operators { get; set; }
        public Queue Queue { get; set; }
        public Server Server { get; set; }

        protected override void ModelTimeChanged(object sender, Models.TimeChangedEventArgs e)
        {
            if (e.NewValue >= NextEventTime)
            {
                NextEventTime = CurrentModelTime + RandomGenerator.NextNormal(((double)60 / Model.ClientsPerHour), 0.8d);
                AddNewClient();
            }
        }

        public Client AddNewClient()
        {
            Client client = new Client(this);
            return client;
        }

    }
}
