﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using SberModel.Models;
using System.Diagnostics;
using System.Windows;
using SberModel.Extensions;

namespace SberModel
{
    public enum StatusEnum
    {
        Newcoming,
        Waiting,
        Coming,
        Working,
        GoingAway
    };

    public class StartDialogEventArgs : EventArgs
    {

    }
    public class Client : VisibleElement
    {
        private Department _department;
        private double _queueingTime;
        private StatusEnum Status;
        private Point _currentPosition;

        public long Number { get; private set; }
        public Operator ClientOperator { get; private set; }

        public event EventHandler<StartDialogEventArgs> DialogStarted;

        public Client(Department department)
        {
            Send(ModelStatus.Empty, "Новый клиент!");
            _department = department;
            NextEventTime = CurrentModelTime + 0.5;
            Subscribe();
        }

        private void SubscribeToOperators()
        {
            if (_department == null || _department.Operators == null)
                return;
            foreach (var op in _department.Operators)
            {
                op.NewClientCall += NewClientCall;
            }
        }

        void NewClientCall(object sender, ClientCallEventArgs e)
        {
            if (e.Number == Number)
            {
                ClientOperator = (sender as Operator);
                Model.AddWaitingTimeStat(CurrentModelTime * 60 - _queueingTime * 60);
                Send(ModelStatus.ClientComming, "Клиент {0}: подхожу к оператору {1}", Number, ClientOperator.Number);
                NextEventTime = CurrentModelTime + 0.3;
                Status = StatusEnum.Coming;
                Subscribe();
            }
        }

        protected override void ModelTimeChanged(object sender, TimeChangedEventArgs e)
        {
            if (e.NewValue >= NextEventTime)
            {
                Unsubcribe();
                if (Status == StatusEnum.Newcoming)
                {
                    Number = JoinQueue();
                    Send(ModelStatus.ClientWaiting, "Клиент {0}: встал в очередь с номером {0}", Number);
                    Status = StatusEnum.Waiting;
                    SubscribeToOperators();
                }
                if (Status == StatusEnum.Coming)
                {
                    if (this != null && ClientOperator != null)
                    {
                        Send(ModelStatus.ClientProcessed, "Клиент {0}: Начинаю диалог с оператором {1}", ClientOperator.Number, ClientOperator.Number);
                        Status = StatusEnum.Working;
                        StartDialog();
                    }
                }
                if (Status == StatusEnum.GoingAway)
                {
                    Send(ModelStatus.ClientFinished);
                    //Dispose();
                }
            }
        }

        public void StartDialog()
        {
            if (DialogStarted != null)
            {
                DialogStarted(this, new StartDialogEventArgs());
                if (this != null && _department != null)
                {
                    foreach (var op in _department.Operators)
                    {
                        op.NewClientCall -= NewClientCall;
                    }
                }
            }
        }

        public void FinishDialog()
        {
            Send(ModelStatus.ClientGoingAway, "Клиент {0}: завершаю диалог с оператором {1}", ClientOperator.Number, ClientOperator.Number);
            Model.ClientsServed++;
            NextEventTime = CurrentModelTime + 0.5d;
            Status = StatusEnum.GoingAway;
            Subscribe();
            //Dispose();
        }

        public int JoinQueue()
        {
            try
            {
                _queueingTime = CurrentModelTime;
                if (_department != null)
                    return _department.Queue.AddClient(this);
                return -1;
            }
            catch (Exception e)
            {
                Debug.Write(e.ToString());
                return -1;
            }
        }

        protected override void Dispatch()
        {
            Unsubcribe();
            _department = null;
            Number = 0;
            ClientOperator = null;
            DialogStarted = null;

            base.Dispatch();
        }

        public override void UpdateView(ModelStatus status)
        {
            try
            {
                if (GraphicPresentations == null)
                    return;
                switch (status)
                {
                    case ModelStatus.Empty:
                        {
                            GraphicPresentations.Add(Model.Drawer.DrawEllipse(30, 30));
                            _currentPosition = new Point(250, 200);
                            Canvas.SetLeft((GraphicPresentations[0] as Ellipse), _currentPosition.X);
                            Canvas.SetTop((GraphicPresentations[0] as Ellipse), _currentPosition.Y);

                            GraphicPresentations.Add(Model.Drawer.DrawText(Id.ToString("X2"), new Point(0d,0d), 30, 30));
                            Canvas.SetLeft((GraphicPresentations[1]), _currentPosition.X);
                            Canvas.SetTop((GraphicPresentations[1]), _currentPosition.Y);
                            Number = _department.Queue.GetNumber();
                            if (Number == -1)
                            {
                                Send(ModelStatus.ClientFinished);
                                Dispose();
                                break;
                            }
                            Send(ModelStatus.ClientQueueing);
                            break;
                        }
                    case ModelStatus.ClientComming:
                        {
                            if (GraphicPresentations == null)
                                break;
                            (GraphicPresentations[0] as Ellipse).Fill = Brushes.CadetBlue;
                            if (GraphicPresentations == null)
                                break;
                            Model.Updater.Add(GraphicPresentations[0] ?? new UIElement(), new Point((ClientOperator.Number - 1) * 50 + 60, 40), NextEventTime);
                            if (GraphicPresentations == null)
                                break;
                            Model.Updater.Add(GraphicPresentations[1] ?? new UIElement(), new Point((ClientOperator.Number - 1) * 50 + 60, 40), NextEventTime);
                            //Canvas.SetLeft((GraphicPresentations[0] as Ellipse), (ClientOperator.Number - 1) * 50 + 10);
                            //Canvas.SetLeft(GraphicPresentations[1], (ClientOperator.Number - 1) * 50 + 10);
                            //Canvas.SetTop((GraphicPresentations[0] as Ellipse), 40);
                            //Canvas.SetTop((GraphicPresentations[1]), 40);
                            break;
                        }
                    case ModelStatus.ClientQueueing:
                        {
                            long rows = Number / 5;
                            long pos = Number - rows * 5;
                            if (GraphicPresentations != null)
                            {
                                Model.Updater.Add(GraphicPresentations[0], new Point(10 + rows * 45, 90 + pos * 35), NextEventTime);
                                //Model.Updater.Add(GraphicPresentations[0], new Point(50,50), NextEventTime);
                                Model.Updater.Add(GraphicPresentations[1], new Point(10 + rows * 45, 90 + pos * 35), NextEventTime);
                            }
                            //Canvas.SetRight((GraphicPresentations[0] as Ellipse), 150);
                            //Canvas.SetBottom((GraphicPresentations[0] as Ellipse), 50);
                            //Canvas.SetRight((GraphicPresentations[1]), 150);
                            //Canvas.SetBottom((GraphicPresentations[1]), 50);
                            break;
                        }
                    case ModelStatus.ClientWaiting:
                        {
                            (GraphicPresentations[0] as Ellipse).Fill = Brushes.Gray;
                            //Model.Updater.Add(GraphicPresentations[0], new Point(100, 100), Model.CurrentTimeInSeconds + 15);
                            //Model.Updater.Add(GraphicPresentations[1], new Point(100, 100), Model.CurrentTimeInSeconds + 15);
                            //Canvas.SetRight((GraphicPresentations[0] as Ellipse), new Random().Next(0, 250));
                            //Canvas.SetBottom((GraphicPresentations[0] as Ellipse), new Random().Next(0, 150));
                            //Canvas.SetRight((GraphicPresentations[1]), new Random().Next(0, 250));
                            //Canvas.SetBottom((GraphicPresentations[1]), new Random().Next(0, 150));
                            break;
                        }
                    case ModelStatus.ClientGoingAway:
                        {
                            if (GraphicPresentations == null)
                                return;
                            foreach (var item in GraphicPresentations)
                            {
                                Model.Updater.Add(item, _currentPosition, NextEventTime);
                            }
                            break;
                        }
                    case ModelStatus.ClientProcessed:
                        {
                            (GraphicPresentations[0] as Ellipse).Fill = Brushes.DarkOrange;
                            break;
                        }
                    case ModelStatus.ClientFinished:
                        {
                            Unsubcribe();
                            GraphicPresentations = null;
                            Dispose();
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                Debug.Write(e.ToString());
            }
        }
    }
}
