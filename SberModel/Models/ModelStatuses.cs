﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel.Models
{
    public enum ModelStatus
    {
        Empty,

        ClientQueueing,
        ClientWaiting,
        ClientComming,
        ClientProcessed,
        ClientGoingAway,
        ClientFinished,

        ServerCreated,
        ServerWaiting,
        ServerWorking,

        OperatorWaiting,
        OperatorCalling,
        OperatorProcessing,
        OperatorSending,

        GlobalTimeOver
    }
}
