﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using SberModel.Models;
using System.Windows;
using System.Windows.Shapes;
using System.Diagnostics;

namespace SberModel
{
    public enum OperatorStatus
    {
        Busy,
        Availiable,
        GotServerResponse,
        UpdatedData
    }
    public class Operator : VisibleElement
    {
        public readonly long Number;
        public OperatorStatus Status { get; private set; }
        private readonly Department _department;
        private Client _currentClient;

        public Operator(Department department, long number)
        {
            Status = OperatorStatus.Availiable;
            Number = number;
            _department = department;
            Subscribe();
            GraphicPresentations.Add(Model.Drawer.DrawRectangle(new Point((number - 1) * 50 + 60, 10), 30, 30));
            GraphicPresentations.Add(Model.Drawer.DrawText(Number.ToString(), new Point((number - 1) * 50 + 60, 10), 30, 30));
        }

        public event EventHandler<ServerRequestEventArgs> NewServerRequest; 
        public event EventHandler<ClientCallEventArgs> NewClientCall;

        private void OnNewClient(ClientCallEventArgs e)
        {
            if (NewClientCall != null)
                NewClientCall(this, e);
        }

        private void OnNewServerRequest(ServerRequestEventArgs e)
        {
            if (NewServerRequest != null)
                NewServerRequest(this, e);
        }

        protected override void ModelTimeChanged(object sender, TimeChangedEventArgs e)
        {
            if (NextEventTime <= e.NewValue)
            {
                if (Status == OperatorStatus.Availiable)
                {
                    NextEventTime = CurrentModelTime + 0.1;
                    CallNewClient();
                    if (Status == OperatorStatus.Busy)
                        NextEventTime = CurrentModelTime + RandomGenerator.NextNormal(((double)Model.OperatorWorkTime) / 60, 0.5d);
                }
                else if (Status == OperatorStatus.Busy)
                {
                    Unsubcribe();
                    SendServerRequest();
                }
            }
        }

        public void SendServerRequest()
        {
            Send(ModelStatus.OperatorSending, "Операционист {0}: отправляю запрос на сервер", Number);
            OnNewServerRequest(new ServerRequestEventArgs(RandomGenerator.NextNormal(1.0d, 0.1d)));
            Server.TaskDone += Server_TaskDone;

        }

        void Server_TaskDone(object sender, TaskDoneEventArgs e)
        {
            if (e.OwnerId == Id)
            {
                Server.TaskDone -= Server_TaskDone;
                GetServerResponse();
            }
        }

        public void GetServerResponse()
        {
            FinishDialog();
        }

        private void FinishDialog()
        {
            try
            {
                if (_currentClient != null)
                    _currentClient.FinishDialog();
            }
            catch (Exception e)
            {
                Debug.Write(e.ToString());
                //Console.WriteLine(Model.NextId);
                //Console.WriteLine(Model.Department.Operators);
                //Model.Pause();
            }
            _currentClient = null;
            Status = OperatorStatus.Availiable;
            NextEventTime = CurrentModelTime;
            Subscribe();
            Send(ModelStatus.OperatorWaiting);
        }

        private void CallNewClient()
        {
            var client = _department.Queue.GetNextClient();
            if (client != null)
            {
                Send(ModelStatus.OperatorCalling, "Операционист {0}: Вызываю клиента {1}", Number, client.Number);
                client.DialogStarted += client_DialogStarted;
                OnNewClient(new ClientCallEventArgs(client.Number));
                Status = OperatorStatus.Busy;
            }
            else
            {
                Send(ModelStatus.OperatorWaiting, "Операционист {0} скучает без клиентов", Number);
            }
        }

        void client_DialogStarted(object sender, StartDialogEventArgs e)
        {
            _currentClient = sender as Client;
            Send(ModelStatus.OperatorProcessing, "Операционист {0}: здравствуйте, клиент {1}", Number, _currentClient.Number);
            if (Status == OperatorStatus.Busy)
                return;
            Status = OperatorStatus.Busy;
            NextEventTime = CurrentModelTime + RandomGenerator.NextNormal(((double)Model.OperatorWorkTime) / 60, 0.5d);
            Subscribe();
        }

        public override void UpdateView(ModelStatus status)
        {
            Rectangle rect = (Rectangle) GraphicPresentations[0];
            switch (status)
            {
                case ModelStatus.OperatorWaiting:
                {
                    rect.Fill = Brushes.GreenYellow;
                    break;
                }
                case ModelStatus.OperatorCalling:
                {
                    rect.Fill = Brushes.Orange;
                    break;
                }
                default:
                {
                    rect.Fill = Brushes.Red;
                    break;
                }
            }
        }
    }
}
