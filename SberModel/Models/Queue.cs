﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel
{
    public class Queue
    {
        private ConcurrentQueue<Client> _queue;
        private ConcurrentDictionary<int?, bool> places = new ConcurrentDictionary<int?,bool>();
        object l = new object();
        private int _lastNumber = 0;
        public Queue()
        {
            _queue = new ConcurrentQueue<Client>();
            for (int i = 0; i < 30; i++)
            {
                places.TryAdd(i, false);
            }
        }
        public int GetNumber()
        {
            int number = places.FirstOrDefault(q => !q.Value).Key ?? -1;
            if (number == -1)
                return -1;
            places[number] = true;
            return number;
        }
        public int AddClient(Client client)
        {
            lock (l)
            {
                _queue.Enqueue(client);
                Model.ClientsInLine++;
                return Convert.ToInt32(client.Number);
            }
        }

        public Client GetNextClient()
        {
            lock (l)
            {
                Client client;
                if (_queue.TryDequeue(out client))
                {
                    places[Convert.ToInt32(client.Number)] = false;
                    Model.ClientsInLine--;
                    return client;
                }
                return null;
            }
        }


        public int Count
        {
            get { return _queue.Count; }
        }
    }
}
