﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SberModel;
using SberModel.Models;

namespace Sber
{
    internal delegate void SetTextCallBack(string text);

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Communication.NewMessageReceived += Communication_NewMessageReceived;
        }

        void Communication_NewMessageReceived(object sender, NewMessageReceivedEventArgs e)
        {
            SetTextCallBack callBack = SetText;
            Invoke(callBack,
                string.Format("{0}: {1}{2}", e.Message.Sender.GetSenderType(), e.Message.Text,
                    Environment.NewLine));
        }

        private void SetText(string text)
        {
            outputText.Text += text;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            //if (!Model.IsInitialized)
            //    Model.Initialize();
            //if (!Model.IsActive)
            //    Model.Start();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            //if (Model.IsActive)
            //    Model.Pause();
        }
    }
}
